﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcKs {
  static class SpanExtensions {
    internal static ReadOnlyMemory<T> ToReadOnlyMemory<T>(this Span<T> self) {
      if (self.Length < 1) return default;

      var result = new T[self.Length];
      self.CopyTo(result.AsSpan());

      return new ReadOnlyMemory<T>(result);
    }

    internal static ReadOnlyMemory<T> ToReadOnlyMemory<T>(this ReadOnlySpan<T> self) {
      if (self.Length < 1) return default;

      var result = new T[self.Length];
      self.CopyTo(result.AsSpan());

      return new ReadOnlyMemory<T>(result);
    }
  }
}
