﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcKs {
  partial struct RoChars {
    public partial struct LazyFields {
      #region LoadMask
      public LoadFlags LoadMask;

      public static bool IsLoaded(ref LazyFields input, LoadFlags flag) => (input.LoadMask & flag) == flag;

      public static LoadFlags SetAsLoaded(ref LazyFields input, LoadFlags flag) => input.LoadMask |= flag;
      public static LoadFlags SetAsNotLoaded(ref LazyFields input, LoadFlags flag) => input.LoadMask &= ~flag;
      #endregion LoadMask

      #region SubString
      public string? SubString;

      public static bool IsSubStringLoaded(ref RoChars input) => IsSubStringLoaded(ref input.lazy);
      public static bool IsSubStringLoaded(ref LazyFields input) => IsLoaded(ref input, LoadFlags.SubString);

      public static void ClearSubString(ref LazyFields input) {
        input.SubString = default;
        SetAsNotLoaded(ref input, LoadFlags.SubString);
      }

      public static string? SetSubString(ref LazyFields input, string? value) {
        input.SubString = value;
        SetAsLoaded(ref input, LoadFlags.SubString);

        return value;
      }

      public static string? GetSubString(ref RoChars input) {
        return IsSubStringLoaded(ref input.lazy)
                ? input.lazy.SubString
                : SetSubString(ref input.lazy, input.ComputeString());
      }
      #endregion SubString

      #region HashCode
      public int HashCode;

      public static bool IsHashCodeLoaded(ref RoChars input) => IsHashCodeLoaded(ref input.lazy);
      public static bool IsHashCodeLoaded(ref LazyFields input) => IsLoaded(ref input, LoadFlags.HashCode);

      public static void ClearHashCode(ref LazyFields input) {
        input.HashCode = default;
        SetAsNotLoaded(ref input, LoadFlags.HashCode);
      }

      public static int SetHashCode(ref LazyFields input, int value) {
        input.HashCode = value;
        SetAsLoaded(ref input, LoadFlags.HashCode);

        return value;
      }

      public static int GetHashCode(ref RoChars input) {
        return IsHashCodeLoaded(ref input.lazy)
                ? input.lazy.HashCode
                : SetHashCode(ref input.lazy, input.ComputeHashCode().ToHashCode());
      }
      #endregion HashCode

      #region ***All
      public static void ClearAll(ref LazyFields input) {
        input.LoadMask = LoadFlags.None;
        input.SubString = default;
        input.HashCode = default;
      }

      public static void PopulateAll(ref RoChars input) {
        GetSubString(ref input);
        GetHashCode(ref input);
      }
      #endregion ***All
    }

    partial struct LazyFields {
      public enum LoadFlags : byte {
        None = 0,
        SubString = 1,
        HashCode = 2
      }
    }
  }
}
