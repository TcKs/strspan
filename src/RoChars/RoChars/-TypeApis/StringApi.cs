﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcKs {
  partial struct RoChars : IEquatable<string> {
    #region Static factory methods (string)
    /// <summary>
    /// Creates new <see cref="RoChars"/> from string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static RoChars From(string source) {
      if (source is null) throw new ArgumentNullException(nameof(source));

      return source.Length < 1 ? Empty : From(source.AsMemory());
    }

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(string source, int offset) {
      if (source is null) throw new ArgumentNullException(nameof(source));

      return From(source.AsMemory(), offset);
    }

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(string source, int offset, int length) {
      if (source is null) throw new ArgumentNullException(nameof(source));

      return From (source.AsMemory(), offset, length);
    }
    #endregion Static factory methods (string)

    #region Equality (string)
    public bool Equals(string? other) => Equals(ref this, other);

    public static bool Equals(string? a, RoChars b) => a is not null && Equals(From(a), ref b);
    public static bool Equals(string? a, ref RoChars b) => a is not null && Equals(From(a), ref b);

    public static bool Equals(RoChars a, string? b) => b is not null && Equals(ref a, From(b));
    public static bool Equals(ref RoChars a, string? b) => b is not null && Equals(ref a, From(b));

    public static bool Equals(string? a, RoChars? b) => Equals(a, ref b);
    public static bool Equals(string? a, ref RoChars? b) {
      var aIsNull = a is null;
      var bIsNull = !b.HasValue;

      if (aIsNull != bIsNull) return false;
      if (aIsNull) return bIsNull;

      return Equals(From(a!), b!.Value);
    }

    public static bool Equals(RoChars? a, string? b) => Equals(b, a);
    public static bool Equals(ref RoChars? a, string? b) => Equals(b, ref a);

    public static bool operator ==(string? a, RoChars b) => Equals(a, b);
    public static bool operator !=(string? a, RoChars b) => !Equals(a, b);

    public static bool operator ==(RoChars a, string? b) => Equals(a, b);
    public static bool operator !=(RoChars a, string? b) => !Equals(a, b);

    public static bool operator ==(string? a, RoChars? b) => Equals(a, b);
    public static bool operator !=(string? a, RoChars? b) => !Equals(a, b);

    public static bool operator ==(RoChars? a, string? b) => Equals(a, b);
    public static bool operator !=(RoChars? a, string? b) => !Equals(a, b);
    #endregion Equality (string)
  }
}
