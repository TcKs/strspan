using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using static TcKs.Helpers;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class ToSpanTests {
    [TestMethod]
    public void ThrowsWithNullString() {
      Assert.ThrowsException<ArgumentNullException>(() => RoChars.From(null as string));
    }

    [TestMethod]
    public void WorksWithEmptyString() {
      var strSpan = RoChars.From(string.Empty);
      var span = strSpan.ToSpan();

      Assert.AreEqual(0, span.Length);
      Assert.IsTrue(span.IsEmpty);
    }

    [TestMethod]
    public void WorksWithFullStringString() {
      var str = "hello world";
      var strSpan = RoChars.From(str);
      var span = strSpan.ToSpan();

      Assert.AreEqual(str.Length, span.Length);
      Assert.IsFalse(span.IsEmpty);

      CheckSameCharacters(str, span);
    }

    [DataTestMethod,
      DataRow(0, 1), DataRow(0, 2), DataRow(0, 3),
      DataRow(1, 1), DataRow(1, 2), DataRow(1, 3),
      DataRow(8, 1), DataRow(8, 2), DataRow(8, 3)
    ]
    public void WorksWithPartialStringString(int offset, int length) {
      var str = "hello world";
      var subStr = str.Substring(offset, length);
      var strSpan = RoChars.From(str, offset, length);
      var span = strSpan.ToSpan();

      Assert.AreEqual(subStr.Length, span.Length);

      CheckSameCharacters(subStr, span);
    }
  }
}