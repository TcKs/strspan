#nullable enable

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class EqualsTests {
    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello"), DataRow(false, "aloha", "hello")]
    public void EqualsWithStrSpansWorks(bool expectedResult, string aStr, string bStr) {
      void RunAsserts(RoChars a, RoChars b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(a, ref b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, ref b));

        Assert.AreEqual(expectedResult, a.Equals(b));
        Assert.AreEqual(expectedResult, a.Equals(ref b));
        Assert.AreEqual(expectedResult, a.Equals((object)b));

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(aSpan, bSpan);
      RunAsserts(bSpan, aSpan);
    }

    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithNullableStrSpansWorks(bool expectedResult, string aStr, string bStr) {
      static void RunAsserts(bool expectedResult, RoChars? a, RoChars? b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(a, ref b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, ref b));

        if (a is not null) {
          Assert.AreEqual(expectedResult, a.Equals(b));
        }

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(expectedResult, aSpan, bSpan);
      RunAsserts(expectedResult, bSpan, aSpan);

      RunAsserts(false, null, aSpan);
      RunAsserts(false, aSpan, null);
      RunAsserts(false, null, bSpan);
      RunAsserts(false, bSpan, null);
    }

    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithStrSpanAndNullableStrSpanWorks(bool expectedResult, string aStr, string bStr) {
      static void RunAsserts(bool expectedResult, RoChars a, RoChars? b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(a, ref b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, ref b));

        Assert.AreEqual(expectedResult, a.Equals(b));
        Assert.AreEqual(expectedResult, a.Equals(ref b));
        Assert.AreEqual(expectedResult, a.Equals((object?)b));

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(expectedResult, aSpan, bSpan);
      RunAsserts(expectedResult, bSpan, aSpan);

      RunAsserts(false, aSpan, null);
      RunAsserts(false, bSpan, null);
    }

    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithNullableStrSpanAndStrSpanWorks(bool expectedResult, string aStr, string bStr) {
      static void RunAsserts(bool expectedResult, RoChars? a, RoChars b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(a, ref b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, ref b));

        if (a is not null) {
          Assert.AreEqual(expectedResult, a.Equals(b));
          Assert.AreEqual(expectedResult, a.Equals((object?)b));
        }

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(expectedResult, aSpan, bSpan);
      RunAsserts(expectedResult, bSpan, aSpan);

      RunAsserts(false, null, aSpan);
      RunAsserts(false, null, bSpan);
    }

    [TestMethod]
    public void EqualsWorksWithSameHashCode() {
      var a = RoChars.From("hello");
      var aFields = RoChars.GetLazyFields(a);
      RoChars.LazyFields.SetHashCode(ref aFields, 123);
      Assert.AreEqual(123, aFields.HashCode);
      Assert.IsTrue(RoChars.LazyFields.IsHashCodeLoaded(ref aFields));

      var b = RoChars.From("aloha");
      var bFields = RoChars.GetLazyFields(b);
      RoChars.LazyFields.SetHashCode(ref bFields, 123);
      Assert.AreEqual(123, bFields.HashCode);
      Assert.IsTrue(RoChars.LazyFields.IsHashCodeLoaded(ref bFields));

      var field = a.GetType().GetField("lazy", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
      object aObj = a;
      field!.SetValue(aObj, aFields);
      a = (RoChars)aObj;

      Assert.AreEqual(123, a.GetHashCode());
      Assert.AreEqual(123, a.GetHashCode());

      object bObj = b;
      field!.SetValue(bObj, bFields);
      b = (RoChars)bObj;

      Assert.AreEqual(123, b.GetHashCode());
      Assert.AreEqual(123, b.GetHashCode());

      Assert.IsFalse(RoChars.Equals(a, b));
    }
  }
}