using Microsoft.VisualStudio.TestTools.UnitTesting;

using LazyFields = TcKs.RoChars.LazyFields;

namespace TcKs.RoCharsTests.LazyFieldsTests {
  [TestClass]
  public class SomeTests {
    [TestMethod]
    public void DefaultLazyFieldsAreNotLoaded() {
      var fields = default(LazyFields);

      Assert.AreEqual(LazyFields.LoadFlags.None, fields.LoadMask);
      Assert.IsFalse(LazyFields.IsSubStringLoaded(ref fields));
      Assert.IsFalse(LazyFields.IsHashCodeLoaded(ref fields));
    }

    [TestMethod]
    public void SetAndClearAllWillSetAllNotLoaded() {
      var fields = default(LazyFields);

      LazyFields.SetSubString(ref fields, "kuku");
      Assert.IsTrue(LazyFields.IsSubStringLoaded(ref fields));
      Assert.AreEqual("kuku", fields.SubString);
      Assert.AreEqual("kuku", fields.SubString);

      LazyFields.SetHashCode(ref fields, 123);
      Assert.IsTrue(LazyFields.IsHashCodeLoaded(ref fields));
      Assert.AreEqual(123, fields.HashCode);
      Assert.AreEqual(123, fields.HashCode);

      LazyFields.ClearAll(ref fields);

      Assert.AreEqual(LazyFields.LoadFlags.None, fields.LoadMask);
      Assert.IsFalse(LazyFields.IsSubStringLoaded(ref fields));
      Assert.IsFalse(LazyFields.IsHashCodeLoaded(ref fields));
    }

    [TestMethod]
    public void ClearSubStringWorks() {
      var fields = default(LazyFields);

      LazyFields.SetSubString(ref fields, "kuku");
      Assert.IsTrue(LazyFields.IsSubStringLoaded(ref fields));
      Assert.AreEqual("kuku", fields.SubString);

      LazyFields.SetHashCode(ref fields, 123);
      Assert.IsTrue(LazyFields.IsHashCodeLoaded(ref fields));
      Assert.AreEqual(123, fields.HashCode);

      LazyFields.ClearSubString(ref fields);

      Assert.IsFalse(LazyFields.IsSubStringLoaded(ref fields));
      Assert.IsTrue(LazyFields.IsHashCodeLoaded(ref fields));
      Assert.AreEqual(123, fields.HashCode);
    }

    [TestMethod]
    public void ClearHashCodeWorks() {
      var fields = default(LazyFields);

      Assert.IsFalse(LazyFields.IsHashCodeLoaded(ref fields));
      LazyFields.SetHashCode(ref fields, 123);
      Assert.IsTrue(LazyFields.IsHashCodeLoaded(ref fields));

      LazyFields.ClearHashCode(ref fields);
      Assert.IsFalse(LazyFields.IsHashCodeLoaded(ref fields));
    }
  }
}
