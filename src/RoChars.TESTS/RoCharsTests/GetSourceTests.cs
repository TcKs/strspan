using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class GetSourceTests {
    [DataTestMethod, DataRow("hello world")]
    public void ByValWorksWithNullString(string input) {
      var span = RoChars.From(input);

      Helpers.CheckSameCharacters(input, RoChars.GetSource(span));
    }

    [DataTestMethod, DataRow("hello world")]
    public void ByRefWorksWithNullString(string input) {
      var span = RoChars.From(input);

      Helpers.CheckSameCharacters(input, RoChars.GetSource(ref span));
    }
  }
}