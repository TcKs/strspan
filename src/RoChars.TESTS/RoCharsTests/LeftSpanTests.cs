using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class LeftSpanTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void LeftSpanWorks(string str) {
      var span = RoChars.From(str);

      Assert.IsTrue(span.LeftSpan(0).IsEmpty);

      if (str is null) return;

      for (var i = 0; i < str.Length; i++) {
        var subStr = str.Substring(0, i);
        var leftSpan = span.LeftSpan(i);

        Assert.AreEqual(subStr.Length, leftSpan.Length);
        Helpers.CheckSameCharacters(subStr, leftSpan);
      }
    }
  }
}