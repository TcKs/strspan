using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class NextTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void NextWorks(string str) {
      var span = RoChars.From(str, 0, 0);

      {
        var rSpan = span.Next();
        Assert.AreEqual(str?.Length ?? 0, rSpan.Length);
      }

      if (str is null) return;

      for (var i = 0; i < str.Length; i++) {
        var subStr = str.Substring(i);
        var nextSpan = RoChars.From(str, 0, i);
        var rightSide = nextSpan.Next();

        Assert.AreEqual(str.Length - i, rightSide.Length);
        Helpers.CheckSameCharacters(subStr, rightSide);
      }
    }
  }
}