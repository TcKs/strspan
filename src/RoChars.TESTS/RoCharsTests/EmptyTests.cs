using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class EmptyTests {
    private void CheckForEmptyness(RoChars span) {
      Assert.IsTrue(span.IsEmpty);
      Assert.AreEqual(0, span.Length);
      Assert.AreEqual(0, span.Offset);
      Assert.IsNotNull(RoChars.GetSource(ref span));
    }

    [TestMethod]
    public void EmptyStrSpanIsEmpty() {
      CheckForEmptyness(RoChars.Empty);
    }

    [TestMethod]
    public void NewStrSpanFromEmptyStringIsEmpty() {
      var span = RoChars.From(string.Empty);

      CheckForEmptyness(span);
    }

    [TestMethod]
    public void StrSpanFromEmptyStringIsEmpty() {
      var span = RoChars.From(string.Empty);

      CheckForEmptyness(span);
    }

    [TestMethod]
    public void StrSpanFromEmptyStringWithZeroOffsetIsEmpty() {
      CheckForEmptyness(RoChars.From(string.Empty, 0));
    }

    [TestMethod]
    public void StrSpanFromEmptyStringWithZeroOffsetAndLengthIsEmpty() {
      CheckForEmptyness(RoChars.From(string.Empty, 0, 0));
    }
  }
}