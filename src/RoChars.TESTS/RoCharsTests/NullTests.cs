using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class NullTests {
    private void CheckForNullness(RoChars span) {
      Assert.IsTrue(span.IsEmpty);
      Assert.AreEqual(0, span.Length);
      Assert.AreEqual(0, span.Offset);
    }

    [TestMethod]
    public void DefaultStrSpanIsEmpty() {
      var span = default(RoChars);

      CheckForNullness(span);
    }

    [TestMethod]
    public void StrSpanFromNullStringIsEmpty() {
      Assert.ThrowsException<ArgumentNullException>(() => RoChars.From(null as string));
    }

    [TestMethod]
    public void StrSpanFromNullStringWithZeroOffsetIsEmpty() {
      Assert.ThrowsException<ArgumentNullException>(() => RoChars.From(null as string, 0));
    }

    [TestMethod]
    public void StrSpanFromNullStringWithZeroOffsetsAndLengthIsEmpty() {
      Assert.ThrowsException<ArgumentNullException>(() => RoChars.From(null as string, 0, 0));
    }
  }
}