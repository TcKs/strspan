using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class SubSpanTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("ab"), DataRow("hello"), DataRow("lorem ipsum")]
    public void SubSpanWorks(string main) {
      var span = RoChars.From(main);

      for (var il = 0; il < main.Length; il++) {
        for (var ir = 0; ir < main.Length - il; ir++) {
          var len = main.Length - il - ir;
          var subStr = main.Substring(il, len);
          var subSpn = span.SubSpan(il, len);

          Assert.AreEqual(subStr.Length, subSpn.Length);
          Assert.AreEqual(subStr, subSpn.ToString());
        }
      }
    }

    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("ab"), DataRow("hello"), DataRow("lorem ipsum")]
    public void SubSpanThroughIndexerWorks(string main) {
      var span = RoChars.From(main);

      for (var il = 0; il < main.Length; il++) {
        for (var ir = 0; ir < main.Length - il; ir++) {
          var len = main.Length - il - ir;
          var subStr = main.Substring(il, len);
          var subSpn = span[il, len];

          Assert.AreEqual(subStr.Length, subSpn.Length);
          Assert.AreEqual(subStr, subSpn.ToString());
        }
      }
    }

    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void SubSpanThrowsBecauseOfOffset(string str) {
      var span = RoChars.From(str);

      var errOffset = (str?.Length ?? -1) + 1;
      for (var i = 1; i < 5; i++) {
        var offset = errOffset + i;

        for (var l = 0; i < 5; i++) {
          Assert.ThrowsException<ArgumentOutOfRangeException>(() => span.SubSpan(offset, l), $"i: {i}, l: {l}");
        }
      }
    }

    [DataTestMethod, DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void SubSpanThrowsBecauseOfLength(string str) {
      var span = RoChars.From(str);

      for (var i = 0; i < span.Length; i++) {
        var len = span.Length - i;

        // no error => ok
        span.SubSpan(i, len);

        for (var l = 1; l <= 5; l++) {
          Assert.ThrowsException<ArgumentOutOfRangeException>(() => span.SubSpan(i, len + l));
        }
      }
    }
  }
}