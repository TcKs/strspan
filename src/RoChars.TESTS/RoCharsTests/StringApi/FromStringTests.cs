#nullable enable

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests.StringApi {
  [TestClass]
  public class FromStringTests {
    [TestMethod]
    public void FromStringWorks() {
      Helpers.CheckCreatedStrSpan(str => RoChars.From(str));
    }

    [TestMethod]
    public void FromStringAndIntWorks() {
      Helpers.CheckCreatedStrSpan((str, offset) => RoChars.From(str, offset));
    }

    [TestMethod]
    public void FromStringAndIntAndIntWorks() {
      Helpers.CheckCreatedStrSpan((str, offset, length) => RoChars.From(str, offset, length));
    }

    [TestMethod]
    public void FromStringAndIntThrows() {
      Helpers.CheckStrSpanFactoryThrows((str, offset) => RoChars.From(str!, offset));
    }

    [TestMethod]
    public void FromStringAndIntAndIntThrows() {
      Helpers.CheckStrSpanFactoryThrows((str, offset, length) => RoChars.From(str!, offset, length));
    }
  }
}