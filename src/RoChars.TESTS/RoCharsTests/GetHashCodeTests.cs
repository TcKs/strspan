#nullable enable

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class GetHashCodeTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("hello world")]
    public void ComputeHashCodeAndHashCodeAreEquals(string str) {
      var span = RoChars.From(str);

      Assert.AreEqual(span.ComputeHashCode().ToHashCode(), span.GetHashCode());
      Assert.IsTrue(RoChars.LazyFields.IsHashCodeLoaded(ref span));
    }
  }
}