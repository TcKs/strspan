using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class CharAtTests {
    [DataTestMethod, DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void CharAtWorks(string str) {
      var span = RoChars.From(str);

      for (var i = 0; i < str.Length; i++) {
        Assert.AreEqual(str[i], span.CharAt(i));
        Assert.AreEqual(str[i], span[i]);
      }
    }

    [DataTestMethod, DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void CharAtThroughIndexerWorks(string str) {
      var span = RoChars.From(str);

      for (var i = 0; i < str.Length; i++) {
        Assert.AreEqual(str[i], span[i]);
        Assert.AreEqual(str[i], span[i]);
      }
    }

    [DataTestMethod, DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void CharAtThrows(string str) {
      var span = RoChars.From(str);

      var errOffset = (str?.Length ?? -1) + 1;
      for (var i = 0; i < 5; i++) {
        var offset = errOffset + i;

        Assert.ThrowsException<ArgumentOutOfRangeException>(() => span.CharAt(offset));
      }
    }

    [DataTestMethod, DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void CharAtThroghIndexerThrows(string str) {
      var span = RoChars.From(str);

      var errOffset = (str?.Length ?? -1) + 1;
      for (var i = 0; i < 5; i++) {
        var offset = errOffset + i;

        Assert.ThrowsException<ArgumentOutOfRangeException>(() => span[offset]);
      }
    }
  }
}