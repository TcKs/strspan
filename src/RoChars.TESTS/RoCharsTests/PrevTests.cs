using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class PrevTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void PrevWorks(string str) {
      var span = RoChars.From(str);

      Assert.IsTrue(span.Prev().IsEmpty);

      if (str is null) return;

      for (var i = 0; i < str.Length; i++) {
        var subStr = str.Substring(0, i);
        var nextSpan = RoChars.From(str, i);
        var leftSide = nextSpan.Prev();

        Assert.AreEqual(i, leftSide.Length);
        Helpers.CheckSameCharacters(subStr, leftSide);
      }
    }
  }
}